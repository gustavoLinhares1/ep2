import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.JPanel;
import javax.swing.ImageIcon;

public class GameMenu extends JPanel{
	
	private ImageIcon titleImage;
	public Rectangle playClassic = new Rectangle (150,150,100,50);
	public Rectangle playKitty   = new Rectangle (150,250,100,50);
	public Rectangle playStar    = new Rectangle (150,350,100,50);
	
	public void gameMenu (Graphics g) {
		g.setColor(Color.white);
		g.drawRect(24, 10, 851, 55);
		titleImage = new ImageIcon("snaketitle.jpg");
		titleImage.paintIcon(this, g, 25, 11);
		g.setColor(Color.WHITE);
		g.drawRect(24, 74, 851, 577);
		g.setColor(Color.black);
		
		Graphics2D g2d = (Graphics2D) g;
		
		Font font = new Font("arial",Font.BOLD,30);
		g.setFont(font);
		g.drawString("Classico", playClassic.x + 19,  playClassic.y + 30);
		g2d.draw(playClassic);
		g.drawString("Kitty", playKitty.x + 19,  playKitty.y + 30);
		g2d.draw(playKitty);
		g.drawString("Playstar", playStar.x + 19,  playStar.y + 30);
		g2d.draw(playStar);
	}
	
	public void gameClassic(Graphics g, int score, int lengthofsnake) {
		g.setColor(Color.white);
		g.drawRect(24, 10, 851, 55);

		titleImage = new ImageIcon("snaketitle.jpg");
		titleImage.paintIcon(this, g, 25, 11);

		g.setColor(Color.WHITE);
		g.drawRect(24, 74, 851, 577);

		g.setColor(Color.black);
		
		g.fillRect(25, 75, 850, 575);
		g.setColor(Color.white);
		g.setFont(new Font("arial",Font.PLAIN,14));
		g.drawString("Scores: "+score, 780, 30);
		
		g.setColor(Color.white);
		g.setFont(new Font("arial",Font.PLAIN,14));
		g.drawString("Tamanho: "+lengthofsnake, 780, 50);
	}
	
}



