import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.util.*;

public class GamePlay extends JPanel implements KeyListener, ActionListener{
	//
	private GameMenu menu;
	
	private int flagBomb = 0;
	//
	private int[] snakeXlenght = new int[750];
	private int[] snakeYlenght = new int[750];
	
	private boolean left = false;
	private boolean right = false;
	private boolean up = false;
	private boolean down = false;
	
	private ImageIcon rightmouth;
	private ImageIcon upmouth;
	private ImageIcon downmouth;
	private ImageIcon leftmouth;
	
	private int lengthofsnake =3;
	private int deathFlag = 0;
	private Timer timer;
	private int delay = 100;
	private ImageIcon snakeimage;

	private static int [] enemyXpos = {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,
								425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,
								800,825,850};
	private int [] enemyYpos = {75,100,125,150,175,200,225,250,275,300,325,350,375,400,
								425,450,475,500,525,550,575,600,625};

	private ImageIcon enemyimage;
	private ImageIcon bigFruit;
	private ImageIcon decreaseFruit;
	private ImageIcon bombFruit;
	
	
	private Random random = new Random();
	
	private int xpos[] = {random.nextInt(34),random.nextInt(34),random.nextInt(34),random.nextInt(34)};
	private int ypos[] = {random.nextInt(23),random.nextInt(23),random.nextInt(23),random.nextInt(23)};
	
	private int score = 0;
	
	private int moves =0;

	public GamePlay() {
		
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer = new Timer (delay,this);
		timer.start();
			
	}
	
	public void paint(Graphics g) {
		
		// TELA
		menu = new GameMenu();
		menu.gameClassic(g,score,lengthofsnake);

		// Posição Inicial
		if(moves == 0) {
			snakeXlenght[2] = 50;
			snakeXlenght[1] = 75;
			snakeXlenght[0] = 100;

			snakeYlenght[2] = 100;
			snakeYlenght[1] = 100;
			snakeYlenght[0] = 100;
		}
		
		rightmouth = new ImageIcon("rightmouth.png");
		rightmouth.paintIcon(this, g, snakeXlenght[0], snakeYlenght[0]);
		
		
		for(int a=0; a< lengthofsnake;a++) {
			if(a== 0 && right) {
				rightmouth = new ImageIcon("rightmouth.png");
				rightmouth.paintIcon(this, g, snakeXlenght[a], snakeYlenght[a]);
			}
			if(a== 0 && left) {
				leftmouth = new ImageIcon("leftmouth.png");
				leftmouth.paintIcon(this, g, snakeXlenght[a], snakeYlenght[a]);
			}
			if(a== 0 && down) {
				downmouth = new ImageIcon("downmouth.png");
				downmouth.paintIcon(this, g, snakeXlenght[a], snakeYlenght[a]);
			}
			if(a== 0 && up) {
				upmouth = new ImageIcon("upmouth.png");
				upmouth.paintIcon(this, g, snakeXlenght[a], snakeYlenght[a]);
			}
			if (a!= 0) {
				snakeimage = new ImageIcon("snakeimage.png");
				snakeimage.paintIcon(this, g, snakeXlenght[a], snakeYlenght[a]);
			}
		}
		
		
		enemyimage = new ImageIcon("enemy.png");
		
		if((enemyXpos[xpos[0]]== snakeXlenght[0]) && (enemyYpos[ypos[0]]== snakeYlenght[0])) {
			score++;
			lengthofsnake++;
			flagBomb =0;
			xpos[0] = random.nextInt(34);
			ypos[0] = random.nextInt(23);
		}
		enemyimage.paintIcon(this,g,enemyXpos[xpos[0]],enemyYpos[ypos[0]]);
		
		bigFruit = new ImageIcon("bigFruit.png");
		
		if((enemyXpos[xpos[1]]== snakeXlenght[0]) && (enemyYpos[ypos[1]]== snakeYlenght[0])) {
			score++;
			score++;
			flagBomb =0;
			lengthofsnake++;
			xpos[1] = random.nextInt(34);
			ypos[1] = random.nextInt(23);
		}
		bigFruit.paintIcon(this,g,enemyXpos[xpos[1]],enemyYpos[ypos[1]]);
	
		decreaseFruit = new ImageIcon("decreaseFruit.png");
		
		if((enemyXpos[xpos[2]]== snakeXlenght[0]) && (enemyYpos[ypos[2]]== snakeYlenght[0])) {
			lengthofsnake=3;
			xpos[2] = random.nextInt(34);
			ypos[2] = random.nextInt(23);
		}
		decreaseFruit.paintIcon(this,g,enemyXpos[xpos[2]],enemyYpos[ypos[2]]);
		
		bombFruit = new ImageIcon("bombFruit.png");
		
		if(((lengthofsnake%3 == 0) && flagBomb == 0)) {
			xpos[3] = random.nextInt(34);
			ypos[3] = random.nextInt(23);
			flagBomb = 1;
		}
		bombFruit.paintIcon(this,g,enemyXpos[xpos[3]],enemyYpos[ypos[3]]);
		
		if((enemyXpos[xpos[3]]== snakeXlenght[0]) && (enemyYpos[ypos[3]]== snakeYlenght[0])) {
			left = false;
			right = false;
			up = false;
			down = false;
			
			g.setColor(Color.white);
			g.setFont(new Font("arial",Font.BOLD, 50));
			g.drawString("Game Over", 300, 300);
			
			deathFlag = 1;
			
			g.setFont(new Font("arial",Font.BOLD, 20));
			g.drawString("Space to Restart", 350, 340);
			
		}
		
		for(int b=1; b<lengthofsnake ;b++) {
			if (snakeXlenght[b] == snakeXlenght[0] && snakeYlenght[b] == snakeYlenght[0]) {
				left = false;
				right = false;
				up = false;
				down = false;
				
				g.setColor(Color.white);
				g.setFont(new Font("arial",Font.BOLD, 50));
				g.drawString("Game Over", 300, 300);
				
				deathFlag = 1;
				
				g.setFont(new Font("arial",Font.BOLD, 20));
				g.drawString("Space to Restart", 350, 340);
			}
		}
		
		g.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		timer.start();
		
		if(right){
			for(int r= lengthofsnake-1;r>=0;r--) {
				snakeYlenght[r+1]= snakeYlenght[r];
			}
			for(int r= lengthofsnake-1;r>=0;r--) {
				if (r==0) {
					snakeXlenght[r]= snakeXlenght[r]+25;
				}
				else {
					snakeXlenght[r]= snakeXlenght[r-1];
				}
				if(snakeXlenght[r]> 850 ){
					snakeXlenght[r] = 25;
				}
			}
			repaint();
			
		}
		if(left){
			for(int r= lengthofsnake-1;r>=0;r--) {
				snakeYlenght[r+1]= snakeYlenght[r];
			}
			for(int r= lengthofsnake-1;r>=0;r--) {
				if (r==0) {
					snakeXlenght[r]= snakeXlenght[r]-25;
				}
				else {
					snakeXlenght[r]= snakeXlenght[r-1];
				}
				if(snakeXlenght[r]< 25 ){
					snakeXlenght[r] = 850;
				}
			}
			repaint();
		}
		if(up){
			for(int r= lengthofsnake-1;r>=0;r--) {
				snakeXlenght[r+1]= snakeXlenght[r];
			}
			for(int r= lengthofsnake-1;r>=0;r--) {
				if (r==0) {
					snakeYlenght[r]= snakeYlenght[r]-25;
				}
				else {
					snakeYlenght[r]= snakeYlenght[r-1];
				}
				if(snakeYlenght[r]< 75 ){
					snakeYlenght[r] = 625;
				}
			}
			repaint();
		}
		if(down){
			for(int r= lengthofsnake-1;r>=0;r--) {
				snakeXlenght[r+1]= snakeXlenght[r];
			}
			for(int r= lengthofsnake-1;r>=0;r--) {
				if (r==0) {
					snakeYlenght[r]= snakeYlenght[r]+25;
				}
				else {
					snakeYlenght[r]= snakeYlenght[r-1];
				}
				if(snakeYlenght[r]>625 ){
					snakeYlenght[r] = 75;
				}
			}
			repaint();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			moves =0;
			score = 0;
			lengthofsnake =3;
			deathFlag = 0;
			repaint();
		}
		
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			if (deathFlag == 0) {
				moves++;
				right = true;
				if(!left) {
					right = true;
				}
				else {
					right = false;
					left = true;
				}
				up = false;
				down = false;
		
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			if (deathFlag == 0) {
				moves++;
				left = true;
				if(!right) {
					left = true;
				}
				else {
					left = false;
					right = true;
				}
				up = false;
				down = false;
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			if (deathFlag == 0) {
					
				moves++;
				up = true;
				if(!down) {
					up = true;
				}
				else {
					up = false;
					down = true;
				}
				left = false;
				right = false;
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			if (deathFlag == 0) {
					
				moves++;
				down = true;
				if(!up) {
					down = true;
				}
				else {
					down = false;
					up = true;
				}
				left = false;
				right = false;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
